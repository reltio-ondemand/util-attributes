package com.reltio.rocs.attribute.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;
import com.reltio.rocs.attribute.builder.AttributeExecuterBuilder;
import com.reltio.rocs.attribute.executor.Executor;
import com.reltio.rocs.delete.dto.Mode;
import com.reltio.rocs.delete.dto.ScanResponseURI;
import com.reltio.rocs.delete.dto.Uri;

/**
 * @author Shivaputrappa Patil
 */
public class AttributeExecuter {

    private static final Logger LOG = LogManager.getLogger(AttributeExecuter.class.getName());

    private AttributeExecuter() {
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        long start = System.currentTimeMillis();

        if (args.length == 0) {
            LOG.error("configuration properties file path is missing in argument!!");
            System.exit(0);
        }

        Properties config = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");

        Util.setHttpProxy(config);
        String outputFile = config.getProperty("OUTPUT_FILE", "output.txt");
        Files.deleteIfExists(Paths.get(outputFile));

        BufferedWriter writer = Util.getBufferedWriter(outputFile);

        validateInput(config, writer);
        Mode mode = getMode(config);

        if (mode.equals(Mode.INVALID)) {
            LOG.error("Invalid Mode in the configuration : " + mode.toString());
            LOG.error("Applicable Modes are [" + Mode.ACTION_ONLY.toString() + "," + Mode.SCAN_AND_ACTION.toString()
                    + "'" + Mode.SCAN_ONLY + "]");
            System.exit(0);
        }

        String uriFile = "";

        BufferedWriter uriWriter = null;

        if (mode.equals(Mode.SCAN_ONLY)) {
            uriFile = config.getProperty("URI_FILE", "uri.txt");
            uriWriter = Util.getBufferedWriter(uriFile);
            uriWriter.flush();
        }

        BufferedReader uriReader = null;

        if (mode.equals(Mode.ACTION_ONLY)) {
            uriReader = Util.getBufferedReader(config.getProperty("URI_FILE"));
        }

        AttributeExecuter util = new AttributeExecuter();

        ReltioAPIService restApi = Util.getReltioService(config);
        writer.newLine();
        writer.flush();

        int count = util.execute(config, writer, restApi, mode, uriWriter, uriReader);

        long end = System.currentTimeMillis();

        long seconds = TimeUnit.MILLISECONDS.toSeconds(end - start);

        Util.close(writer, uriWriter, uriReader);

        LOG.info("Record no is = " + count + " in " + seconds + " seconds \nProcess Ended ");
    }

    /**
     * This get the MODE for the operation;
     */
    private static Mode getMode(Properties properties) {
        String mode = properties.getProperty("MODE", "SCAN_AND_ACTION");

        switch (mode) {
            case "SCAN_ONLY":
                return Mode.SCAN_ONLY;
            case "SCAN_AND_ACTION":
                return Mode.SCAN_AND_ACTION;
            case "ACTION_ONLY":
                return Mode.ACTION_ONLY;
            default:
                return Mode.INVALID;
        }
    }

    /**
     * This will validate required inputs mentioned in property file
     *
     * @param writer
     * @throws Exception
     */
    private static void validateInput(Properties config, BufferedWriter writer) throws Exception {

        List<String> inputs = new ArrayList<>(Arrays.asList("ENVIRONMENT_URL", "TENANT_ID",
                "AUTH_URL", "ATTRIBUTE_KEY", "ACTION", "FILTER", "MODE"));

        String action = config.getProperty("ACTION").trim().toLowerCase();

        if ("addnew".equals(action) || "findandremove".equals(action)) {
            inputs.add("ATTRIBUTE_VALUE");
        } else if ("findandreplace".equals(action)) {
            inputs.add("ATTRIBUTE_OLD_VALUE");
            inputs.add("ATTRIBUTE_NEW_VALUE");
        } else if ("replaceall".equals(action)) {
            inputs.add("ATTRIBUTE_NEW_VALUE");
        }

        Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();

        mutualExclusiveProps.put(Arrays.asList(new String[]{"PASSWORD", "USERNAME"}), Arrays.asList(new String[]{"CLIENT_CREDENTIALS"}));

        List<String> missings = Util.listMissingProperties(config,
                inputs, mutualExclusiveProps);

        if (!missings.isEmpty()) {
            LOG.error("Please mention missing properties to execute , Action " + action + "\nMissing properties are : \n" + String.join("\n", missings));
            System.exit(0);
        }

        StringBuffer data = new StringBuffer();

        data.append("Process started with below configuration \n\n");

        for (String key : inputs) {
            if (!"PASSWORD".equals(key) && !"CLIENT_CREDENTIALS".equals(key)) {
                data.append(key + " = " + config.getProperty(key) + "\n");
            }
        }

        LOG.info(data);
        writer.write(data.toString());
        writer.flush();
    }

    public static void printPerformance(long totalTasksExecuted, long totalTasksExecutionTime, long programStartTime,
                                        long numberOfThreads) {
        LOG.info("[Performance]: ============= Current performance status (" + new Date().toString()
                + ") =============");
        long finalTime = System.currentTimeMillis() - programStartTime;
        LOG.info("[Performance]:  Total processing time : " + finalTime);
        LOG.info("[Performance]:  Records Processed: " + totalTasksExecuted);
        LOG.info("[Performance]:  Total OPS (Records Processed / Time spent from program start): "
                + (totalTasksExecuted / (finalTime / 1000f)));
        LOG.info("[Performance]:  last Batch Throughput : "
                + (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
        LOG.info(
                "[Performance]: ===============================================================================================================");

    }

    /**
     * This method will add attribute to all matching entities of the configuration
     * mentioned
     *
     * @param config
     * @param writer
     * @param restApi
     * @return count
     * @throws Exception
     */
    protected int execute(Properties config, BufferedWriter writer, ReltioAPIService restApi, Mode mode,
                          BufferedWriter uriWriter, BufferedReader uriReader) throws Exception {

        long programStartTime = System.currentTimeMillis();

        int threads = NumberUtils.toInt(config.getProperty("THREAD_COUNT"), 100);
        int size = Integer.parseInt(config.getProperty("RECORDS_PER_POST", "20"));
        long totalFuturesExecutionTime = 0L;

        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threads);

        List<Future<Long>> futures = new ArrayList<Future<Long>>();

        Gson gson = new Gson();

        boolean isDone = false;

        String intialJSON = "";

        String apiUrl = getApiUl(config);

        final String totalUrl = apiUrl + "entities/_total?filter=" + deriveFilter(config);

        String totalscanUrlResponse = restApi.get(totalUrl);

        LOG.info("total entities = " + totalscanUrlResponse);

        String scanUrl = getSearchUrl(config, "scan");
        int count = 0;

        while (!isDone) {

            for (int i = 0; i < threads; i++) {
                String response = null;
                Executor executor = null;

                if (mode.equals(Mode.ACTION_ONLY)) {
                    List<String> uriListFromFile = new ArrayList<>();
                    for (int batchCount = 0; batchCount < size; batchCount++) {
                        response = uriReader.readLine();
                        if (response != null) {
                            uriListFromFile.add(response);
                        } else {
                            isDone = true;
                            break;
                        }

                    }
                    count += uriListFromFile.size();

                    executor = AttributeExecuterBuilder.build(config, writer, restApi, uriListFromFile, mode,
                            uriReader);

                    futures.add(executorService.submit(executor));
                    if (isDone)
                        break;
                    // uriListFromFile.clear();

                } else {

                    response = restApi.post(scanUrl, intialJSON);
                    if (!Util.isEmpty(response)) {

                        ScanResponseURI scanResponseObjUri = gson.fromJson(response, ScanResponseURI.class);
                        final List<Uri> uris = scanResponseObjUri.getObjects();
                        if (Util.isNotEmpty(uris)) {
                            count += uris.size();

                            if (mode.equals(Mode.SCAN_AND_ACTION)) {
                                executor = AttributeExecuterBuilder.build(config, writer, restApi, uris, mode);
                            } else {
                                executor = AttributeExecuterBuilder.build(config, writer, restApi, uris, mode,
                                        uriWriter);
                            }

                            futures.add(executorService.submit(executor));

                        } else {

                            isDone = true;
                            break;
                        }

                        scanResponseObjUri.setObjects(null);

                        intialJSON = gson.toJson(scanResponseObjUri.getCursor());

                        intialJSON = "{\"cursor\":" + intialJSON + "}";
                    }
                }

            }

            totalFuturesExecutionTime += waitForTasksReady(futures, threads * 2);
            printPerformance(executorService.getCompletedTaskCount() * size, totalFuturesExecutionTime,
                    programStartTime, Integer.parseInt(config.getProperty("THREAD_COUNT")));

        }

        totalFuturesExecutionTime += waitForTasksReady(futures, 0);
        printPerformance(executorService.getCompletedTaskCount() * size, totalFuturesExecutionTime, programStartTime,
                Integer.parseInt(config.getProperty("THREAD_COUNT")));

        executorService.shutdown();

        return count;

    }

    /**
     * This method will returns either total URL or scan URL based on the properties
     * mentioned in configuration file
     */
    protected String getSearchUrl(Properties config, String type) {

        StringBuffer url = new StringBuffer(getApiUl(config));

        int size = Integer.parseInt(config.getProperty("RECORDS_PER_POST", "20"));

        if ("scan".equals(type)) {

            url.append("entities/_scan?filter=");
            url.append(deriveFilter(config));
            url.append("&select=uri,crosswalks,attributes." + config.getProperty("ATTRIBUTE_KEY") + "&max=" + size);

        } else if ("total".equals(type)) {

            url.append("entities/_total?filter=");
            url.append(deriveFilter(config));
        }

        return url.toString();
    }

    protected String deriveFilter(Properties config) {

        return config.getProperty("FILTER");
    }

    /**
     * This will remove completed tasks in collection;
     */
    public long waitForTasksReady(Collection<Future<Long>> futures, int maxNumberInList) throws Exception {

        long totalResult = 0l;
        while (futures.size() > maxNumberInList) {
            Thread.sleep(20);

            for (Iterator<Future<Long>> iterator = futures.iterator(); iterator.hasNext(); ) {
                Future<Long> future = iterator.next();

                if (future.isDone()) {
                    totalResult += future.get();
                    iterator.remove();
                }
            }
        }

        return totalResult;
    }

    /**
     * This will prepare api url from ENVIRONMENT_URL and TENANT_ID
     */
    protected String getApiUl(Properties config) {
        return "https://" + config.getProperty("ENVIRONMENT_URL") + "/reltio/api/" + config.getProperty("TENANT_ID") + "/";
    }
}