/**
 *
 */
package com.reltio.rocs.attribute.executor;

import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.rocs.delete.dto.Attribute;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Mode;
import com.reltio.rocs.delete.dto.Uri;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author spatil
 */
public class ReplaceAllAttributeExecutor extends Executor {

    private static final String DATA = "{\"value\":\"VALUE\"}";

    /*
     * (non-Javadoc)
     *
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Long call() throws Exception {

        String apiUrl = getApiUl(config);

        String key = config.getProperty("ATTRIBUTE_KEY");
        String oldValue = config.getProperty("ATTRIBUTE_OLD_VALUE");
        String newValue = config.getProperty("ATTRIBUTE_NEW_VALUE");
        String source = config.getProperty("SOURCE_SYSTEM");

        String data = prepareRequest(newValue);

        Map<String, String> requestHeaders = new HashMap<>();

        requestHeaders.put("Content-Type", "application/json");

        if (source != null) {
            requestHeaders.put("Source-System", source);
        }

        if (mode.equals(Mode.ACTION_ONLY)) {
            for (String uri : urisFromFile) {
                replace(uri, requestHeaders, data);
            }

        } else {

            for (Uri uri : uris) {

            	Crosswalk crossWalk = getCrossWalk(source, uri);
                Map<String, List<Attribute>> attributesMap = uri.getAttributes();

                List<Attribute> attributes = null;

                if ((attributes = attributesMap.get(key)) == null || attributesMap.get(key).isEmpty()) {
                    continue;
                }

                for (Attribute attr : attributes) {
                    replace(apiUrl, requestHeaders, attr, crossWalk, key, data, oldValue);
                }
            }
        }

        return System.currentTimeMillis();
    }

    protected String replace(String apiUrl, Map<String, String> requestHeaders, String data) throws Exception {

        String response = null;
        try {
            response = restApi.put(apiUrl, requestHeaders, data);
            writer.write(apiUrl + " -- " + response + "\n");
        } catch (ReltioAPICallFailureException e) {
            writer.write(apiUrl + " -- " + e.getErrorResponse() + "\n");
        } catch (Exception e) {
            writer.write(apiUrl + " -- " + e.getMessage() + "\n");
        } finally {
            writer.flush();
        }

        return response;

    }

    protected String replace(String apiUrl, Map<String, String> requestHeaders, Attribute uri, Crosswalk crossWalk,
                             String key, String data, String oldValue) throws Exception {

        StringBuffer attrUrl = new StringBuffer(apiUrl).append(uri.getUri());

        if (crossWalk != null) {
            attrUrl.append("?crosswalkValue=").append(crossWalk.getValue());
        }
        
        if(crossWalk.getSourceTable()!=null) {
        	attrUrl.append("&crosswalkSourceTable=").append(crossWalk.getSourceTable());
        }

        String response = null;
        try {
            if (mode.equals(Mode.SCAN_ONLY)) {
                uriWriter.write(attrUrl.toString());
                uriWriter.newLine();
            } else {
                response = restApi.put(attrUrl.toString(), requestHeaders, data);
                writer.write(attrUrl + " -- " + response + "\n");
            }
        } catch (ReltioAPICallFailureException e) {
            writer.write(attrUrl + " -- " + e.getErrorResponse() + "\n");
        } catch (Exception e) {
            writer.write(attrUrl + " -- " + e.getMessage() + "\n");
        } finally {
            writer.flush();
        }

        return response;

    }

    protected String prepareRequest(String value) {

        return DATA.replaceFirst("VALUE", value);

    }
}