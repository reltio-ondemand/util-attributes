/**
 * 
 */
package com.reltio.rocs.attribute.executor;

import java.util.Map;

import com.reltio.rocs.delete.dto.Attribute;
import com.reltio.rocs.delete.dto.Crosswalk;

/**
 * @author spatil
 *
 */
public class ReplaceMatchingAttributeExecutor extends ReplaceAllAttributeExecutor {

	@Override
	protected String replace(String apiUrl, Map<String, String> requestHeaders, Attribute uri, Crosswalk crossWalk,
			String key, String data, String oldValue) throws Exception {

		if (uri.getValue().equals(oldValue)) {
			return super.replace(apiUrl, requestHeaders, uri, crossWalk, key, data, oldValue);
		}

		return null;
	}
}
