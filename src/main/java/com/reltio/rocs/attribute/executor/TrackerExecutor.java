package com.reltio.rocs.attribute.executor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class TrackerExecutor extends Thread {

    private static final Logger logger = LogManager.getLogger(TrackerExecutor.class.getName());


    int lastCount = 0;
    volatile boolean exit = false;
    long st = System.currentTimeMillis();
    private AtomicInteger count;

    public void setAtomicInteger(AtomicInteger count) {
        this.count = count;
    }

    @Override
    public void run() {

        while (!exit) {

            try {
                sleep(6000);
            } catch (Exception e) {
            }

            int currentCount = count.get();
            double executed = currentCount - lastCount;
            lastCount = currentCount;
            long totalTimeInSeconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - st);
            logger.info("[Performance] Overall OPS = " + (count.get() / totalTimeInSeconds));
            logger.info("[Performance] Current OPS = " + executed / 60);
        }

    }

    public void exitThread() {
        exit = true;
    }

}
