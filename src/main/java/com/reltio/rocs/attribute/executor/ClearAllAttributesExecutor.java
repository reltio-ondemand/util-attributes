/**
 *
 */
package com.reltio.rocs.attribute.executor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.util.Util;
import com.reltio.rocs.delete.dto.Attribute;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Mode;
import com.reltio.rocs.delete.dto.Uri;

/**
 * @author spatil
 */
public class ClearAllAttributesExecutor extends Executor {

	private static final Logger LOGGER
	= LogManager.getLogger(ClearAllAttributesExecutor.class.getName());

	@Override
	public Long call() throws Exception {

		String key = config.getProperty("ATTRIBUTE_KEY");

		Map<String, String> requestHeaders = new HashMap<>();

		requestHeaders.put("Content-Type", "application/json");
		String source = config.getProperty("SOURCE_SYSTEM");

		if (source != null) {
			requestHeaders.put("Source-System", source);
		}
		if (mode.equals(Mode.ACTION_ONLY)) {
			for (String uri : urisFromFile) {
				clearAttribute(uri, requestHeaders);
			}

		} else {

			for (Uri uri : uris) {

				Map<String, List<Attribute>> attributesMap = uri.getAttributes();
				List<Attribute> attributes = null;

				Crosswalk crosswalk = getCrossWalk(source, uri);
				List<Attribute> atr = null;
				String nestedKeys[] = {};

				int nestedKeysLength = nestedKeys.length;

				if (nestedKeysLength > 0) {

					atr = attributesMap.get(nestedKeys[0]);

					for (int i = 1; i < nestedKeysLength; i++) {
						atr = getAttributes(atr, nestedKeys[i]);
					}

					for (Object object : atr) {

						String deepNestedUri = ((Map) object).get("uri").toString();
						String value = ((Map) object).get("value").toString();

						clearAttribute(requestHeaders, crosswalk, deepNestedUri, uri, value);
					}

				} else {

					if ((attributes = attributesMap.get(key)) == null || attributesMap.get(key).isEmpty()) {
						continue;
					}

					for (Attribute attr : attributes) {

						clearAttribute(requestHeaders, crosswalk, attr.getUri(), uri, attr.getValue().toString());
					}
				}
			}
		}

		return System.currentTimeMillis();

	}

	/**
	 * This will delete attribute for the attribute with/without crosswalk
	 */
	protected void clearAttribute(Map<String, String> requestHeaders, Crosswalk crosswalkId, Attribute attr)
			throws Exception {

		String apiUrl = getApiUl(config);
		String restUrl = apiUrl + attr.getUri();

		try {

			if (crosswalkId != null) {
				restUrl = restUrl + "?crosswalkValue=" + crosswalkId;
			}
			if (mode.equals(Mode.SCAN_ONLY)) {
				uriWriter.write(restUrl);
				uriWriter.newLine();
			} else {
				String response = restApi.delete(restUrl, requestHeaders, "");
				writer.write(restUrl + " -- " + response + "\n");
			}
		} catch (ReltioAPICallFailureException e) {
			writer.write(restUrl + " -- " + e.getErrorResponse() + "\n");
		} catch (Exception e) {
			writer.write(restUrl + " -- " + e.getMessage() + "\n");
		} finally {
			writer.flush();
		}
	}

	protected void clearAttribute(String uri, Map<String, String> requestHeaders) throws Exception {

		try {

			String response = restApi.delete(uri, requestHeaders, "");
			writer.write(uri + " -- " + response + "\n");

		} catch (ReltioAPICallFailureException e) {
			writer.write(uri + " -- " + e.getErrorResponse() + "\n");
		} catch (Exception e) {
			writer.write(uri + " -- " + e.getMessage() + "\n");
		} finally {
			writer.flush();
		}
	}

	protected void clearAttribute(Map<String, String> requestHeaders, Crosswalk crosswalkId, String uri, Uri entityUri,
			String value) throws Exception {

		String apiUrl = getApiUl(config);

		String restUrl = apiUrl + uri;
		Crosswalk crosswalk = null;

		try {

			if (crosswalkId != null) {
				restUrl = restUrl + "?crosswalkValue=" + crosswalkId.getValue();
				restUrl = crosswalkId.getSourceTable() != null
						? (restUrl += "&crosswalkSourceTable=" + crosswalkId.getSourceTable())
						: restUrl;
			} else {
				crosswalk = getUriCrossWalk(uri, entityUri);

				restUrl = restUrl + "?crosswalkValue=" + crosswalk.getValue();

				restUrl = crosswalk.getSourceTable() != null
						? (restUrl += "&crosswalkSourceTable=" + crosswalk.getSourceTable())
						: restUrl;

				requestHeaders.put("Source-System", crosswalk.getType());
			}

			if (mode.equals(Mode.SCAN_ONLY)) {
				uriWriter.write(restUrl);
				uriWriter.newLine();
			} else {
				writer.write(new Date() + ", Before Api call : " + restUrl + "\n");
				String response = restApi.delete(restUrl, requestHeaders, "");
				writer.write(new Date() + ", After Api call : " + restUrl + " -- " + response + "\n");
			}
		} catch (ReltioAPICallFailureException e) {
			writer.write(new Date() + restUrl + " -- " + e.getErrorResponse() + "\n");
		} catch (Exception e) {
			writer.write(new Date() + restUrl + " -- " + e.getMessage() + "\n");
		} finally {
			writer.flush();
		}
	}

	/**
	 * */
	protected Crosswalk getUriCrossWalk(String atrUri, Uri uri) {

		Crosswalk crosswalk = new Crosswalk();

		for (Crosswalk walk : uri.getCrosswalks()) {

			if (walk.getAttributes().contains(atrUri)) {
				crosswalk.setType(walk.getType());
				crosswalk.setValue(walk.getValue());
				break;
			}

		}

		return crosswalk;

	}

	protected List<Attribute> getAttributes(List<Attribute> atrs, String atr) {
		List<Attribute> result = new ArrayList<>();

		for (Attribute attr : atrs) {
			Map<String, List<Attribute>> atrss = (Map<String, List<Attribute>>) attr.getValue();
			try {
				List<Attribute> values = atrss.get(atr);

				if (Util.isNotEmpty(values)) {
					result.addAll(values);
				}
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage());
			}
		}

		return result;
	}
}
