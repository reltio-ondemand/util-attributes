/**
 *
 */
package com.reltio.rocs.attribute.executor;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Mode;
import com.reltio.rocs.delete.dto.Uri;

import java.io.BufferedWriter;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;

/**
 * @author spatil
 */
public abstract class Executor implements Callable<Long> {

    protected BufferedWriter writer;
    protected BufferedWriter uriWriter;
    protected ReltioAPIService restApi;
    protected Properties config;
    protected List<Uri> uris = Collections.emptyList();
    protected List<String> urisFromFile = Collections.emptyList();
    protected Mode mode = Mode.SCAN_AND_ACTION;

    public List<String> getUrisFromFile() {
        return urisFromFile;
    }

    public void setUrisFromFile(List<String> urisFromFile) {
        this.urisFromFile = urisFromFile;
    }

    public BufferedWriter getUriWriter() {
        return uriWriter;
    }

    public void setUriWriter(BufferedWriter uriWriter) {
        this.uriWriter = uriWriter;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public List<Uri> getUris() {
        return uris;
    }

    public void setUris(List<Uri> uris) {
        this.uris = uris;
    }

    public BufferedWriter getWriter() {
        return writer;
    }

    public void setWriter(BufferedWriter writer) {
        this.writer = writer;
    }

    public ReltioAPIService getRestApi() {
        return restApi;
    }

    public void setRestApi(ReltioAPIService restApi) {
        this.restApi = restApi;
    }

    public Properties getConfig() {
        return config;
    }

    public void setConfig(Properties config) {
        this.config = config;
    }

    protected Crosswalk getCrossWalk(String source, Uri uri) {

    	Crosswalk crosswalk = null;

        if (source == null || "Reltio".equals(source)) {
            return null;
        }

        String quilifiedSource = "configuration/sources/" + source;

        for (Crosswalk walk : uri.getCrosswalks()) {
            if (walk.getType().equals(quilifiedSource)) {
                crosswalk = walk;
                break;
            }
        }

        return crosswalk;
    }

    /**
     * This will prepare api url from ENVIRONMENT_URL and TENANT_ID
     */
    protected String getApiUl(Properties config) {
        return "https://"+config.getProperty("ENVIRONMENT_URL") + "/reltio/api/" + config.getProperty("TENANT_ID") + "/";
    }
}