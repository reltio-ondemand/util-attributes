package com.reltio.rocs.attribute.executor;

import java.util.Map;

import com.reltio.rocs.delete.dto.Attribute;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Uri;

public class ClearMatchingAttributesExecutor extends ClearAllAttributesExecutor {

	@Override
	protected void clearAttribute(Map<String, String> requestHeaders, Crosswalk crosswalk, Attribute attr)
			throws Exception {
		String attributeValue = config.getProperty("ATTRIBUTE_VALUE");

		if (attributeValue.equals(attr.getValue().toString())) {
			super.clearAttribute(requestHeaders, crosswalk, attr);
		}
	}

	protected void clearAttribute(Map<String, String> requestHeaders, Crosswalk crosswalk, String uri, Uri entityUri,
			String value) throws Exception {
		if (value.equals(config.get("ATTRIBUTE_VALUE"))) {

			super.clearAttribute(requestHeaders, crosswalk, uri, entityUri, value);
		}
	}

}