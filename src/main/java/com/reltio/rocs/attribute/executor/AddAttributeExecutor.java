/**
 *
 */
package com.reltio.rocs.attribute.executor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.rocs.delete.dto.Crosswalk;
import com.reltio.rocs.delete.dto.Mode;
import com.reltio.rocs.delete.dto.Uri;

/**
 * @author spatil
 */
public class AddAttributeExecutor extends Executor {

 

    private static final String DATA = "[{\"value\":\"VALUE\"}]";

    protected String restData = "";

    /*
     * (non-Javadoc)
     *
     * @see java.util.concurrent.Callable#call()
     */
    @Override
    public Long call() throws Exception {

        String apiUrl = getApiUl(config);

        String key = config.getProperty("ATTRIBUTE_KEY");
        String value = config.getProperty("ATTRIBUTE_VALUE");
        String source = config.getProperty("SOURCE_SYSTEM");
 
        prepareRequest(value);

        Map<String, String> requestHeaders = new HashMap<>();

        requestHeaders.put("Content-Type", "application/json");

        if (source != null) {
            requestHeaders.put("Source-System", source);
        }
        if (mode.equals(Mode.ACTION_ONLY)) {
            for (String uri : urisFromFile) {
                pushToReltio(uri, requestHeaders);
            }
        } else {

            for (Uri uri : uris) {

            	Crosswalk crossWalk = getCrossWalk(source, uri);
                pushToReltio(apiUrl, requestHeaders, uri, crossWalk, key, value);
            }
        }

        return System.currentTimeMillis();
    }

    /**
     * This will post request to reltio to add attribute
     *
     * @param uri
     * @param value
     * @param key
     * @param value
     * @throws IOException
     */
    protected String pushToReltio(String apiUrl, Map<String, String> requestHeaders, Uri uri, Crosswalk crossWalk,
                                  String key, String value) throws IOException {

        StringBuffer attrUrl = new StringBuffer(apiUrl).append(uri.getUri()).append("/attributes/").append(key);

        if (crossWalk != null) {
            attrUrl = attrUrl.append("?crosswalkValue=").append(crossWalk.getValue());
        }

        String response = null;

        try {
            if (mode.equals(Mode.SCAN_ONLY)) {
                uriWriter.write(attrUrl.toString());
                uriWriter.newLine();
            } else {
                response = restApi.post(attrUrl.toString(), requestHeaders, restData);

                writer.write(attrUrl + " -- " + response + "\n");
            }
        } catch (ReltioAPICallFailureException e) {
            writer.write(attrUrl + " -- " + e.getErrorResponse() + "\n");
        } catch (Exception e) {
            writer.write(attrUrl + " -- " + e.getMessage() + "\n");
        } finally {
            writer.flush();
        }

        return response;
    }


    protected String pushToReltio(String uri, Map<String, String> requestHeaders) throws IOException {


        String response = null;

        try {

            response = restApi.post(uri, requestHeaders, restData);
            writer.write(uri + " -- " + response + "\n");

        } catch (ReltioAPICallFailureException e) {
            writer.write(uri + " -- " + e.getErrorResponse() + "\n");
        } catch (Exception e) {
            writer.write(uri + " -- " + e.getMessage() + "\n");
        } finally {
            writer.flush();
        }

        return response;
    }

    protected void prepareRequest(String value) {
        restData = DATA.replaceFirst("VALUE", value);
    }
}
