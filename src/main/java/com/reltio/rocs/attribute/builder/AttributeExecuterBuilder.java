/**
 *
 */
package com.reltio.rocs.attribute.builder;

import com.reltio.cst.service.ReltioAPIService;
import com.reltio.rocs.attribute.executor.AddAttributeExecutor;
import com.reltio.rocs.attribute.executor.ClearAllAttributesExecutor;
import com.reltio.rocs.attribute.executor.ClearMatchingAttributesExecutor;
import com.reltio.rocs.attribute.executor.Executor;
import com.reltio.rocs.attribute.executor.ReplaceAllAttributeExecutor;
import com.reltio.rocs.attribute.executor.ReplaceMatchingAttributeExecutor;
import com.reltio.rocs.delete.dto.Mode;
import com.reltio.rocs.delete.dto.Uri;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.List;
import java.util.Properties;

/**
 * @author spatil
 */
public class AttributeExecuterBuilder {

    /***
     * This method will create executed based on the action
     **/
    public static Executor build(Properties config, BufferedWriter writer, ReltioAPIService restApi, List<Uri> uris, Mode mode)
            throws Exception {

        String action = config.getProperty("ACTION").trim().toLowerCase();

        Executor executor = createExecutor(action);

        executor.setConfig(config);
        executor.setRestApi(restApi);
        executor.setWriter(writer);
        executor.setUris(uris);
        executor.setMode(mode);

        return executor;
    }

    public static Executor build(Properties config, BufferedWriter writer, ReltioAPIService restApi, List<String> uris, Mode mode,
                                 BufferedReader uriReader)
            throws Exception {

        String action = config.getProperty("ACTION").trim().toLowerCase();

        Executor executor = createExecutor(action);

        executor.setConfig(config);
        executor.setRestApi(restApi);
        executor.setWriter(writer);
        executor.setMode(mode);
        executor.setUrisFromFile(uris);

        return executor;
    }

    public static Executor build(Properties config, BufferedWriter writer, ReltioAPIService restApi, List<Uri> uris, Mode mode,
                                 BufferedWriter uriWriter)
            throws Exception {

        String action = config.getProperty("ACTION").trim().toLowerCase();

        Executor executor = createExecutor(action);

        executor.setConfig(config);
        executor.setRestApi(restApi);
        executor.setWriter(writer);
        executor.setUriWriter(uriWriter);
        executor.setUris(uris);
        executor.setMode(mode);

        return executor;
    }

    /***
     * This method will create executed based on the action
     **/
    private static Executor createExecutor(String action) throws Exception {

        Executor executor = null;

        if ("addnew".equals(action)) {
            executor = new AddAttributeExecutor();
        } else if ("removeall".equals(action)) {
            executor = new ClearAllAttributesExecutor();
        } else if ("findandremove".equals(action)) {
            executor = new ClearMatchingAttributesExecutor();
        } else if ("replaceall".equals(action)) {
            executor = new ReplaceAllAttributeExecutor();
        } else if ("findandreplace".equals(action)) {
            executor = new ReplaceMatchingAttributeExecutor();
        } else {
            throw new Exception("Invalid action " + action);
        }

        return executor;
    }

}
