
package com.reltio.rocs.delete.dto;

import java.util.Collections;
import java.util.List;

public class Attributes {

	private List<Attribute> attribute = Collections.emptyList();

	public List<Attribute> getAttribute() {
		return attribute;
	}
 
	public void setAttribute(List<Attribute> attribute) {
		this.attribute = attribute;
	}

}
