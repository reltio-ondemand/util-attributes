package com.reltio.rocs.delete.dto;

public class Entity {

	private String uri;

	private String type;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
