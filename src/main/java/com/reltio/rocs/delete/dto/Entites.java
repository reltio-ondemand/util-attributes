package com.reltio.rocs.delete.dto;

import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class Entites {

	int index;

	@SerializedName("object")
	private Entity entitiy;
	
	private Map<String, String> errors;
	
	private Crosswalk crosswalk;

	public Crosswalk getCrosswalk() {
		return crosswalk;
	}

	public void setCrosswalk(Crosswalk crosswalk) {
		this.crosswalk = crosswalk;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Entity getEntitiy() {
		return entitiy;
	}

	public void setEntitiy(Entity entitiy) {
		this.entitiy = entitiy;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Entites [index=").append(index).append(", entitiy=").append(entitiy).append(", errors=")
				.append(errors).append(", crosswalk=").append(crosswalk).append("]");
		return builder.toString();
	}
}
