package com.reltio.rocs.delete.dto;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Uri {
	
	private String uri;
	
	private List<Crosswalk> crosswalks;

	private Map<String, List<Attribute>> attributes = Collections.emptyMap();
	
	public Map<String, List<Attribute>> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, List<Attribute>> attributes) {
		this.attributes = attributes;
	}

	public List<Crosswalk> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalk> crosswalks) {
		this.crosswalks = crosswalks;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Uri [uri=").append(uri).append(", crosswalks=").append(crosswalks).append(", attributes=")
				.append(attributes).append("]");
		return builder.toString();
	}
}