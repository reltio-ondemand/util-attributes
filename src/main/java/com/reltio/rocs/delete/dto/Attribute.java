
package com.reltio.rocs.delete.dto;

public class Attribute {

	private String type;

	private Boolean ov;

	private Object value;

	private String uri;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getOv() {
		return ov;
	}

	public void setOv(Boolean ov) {
		this.ov = ov;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

}
