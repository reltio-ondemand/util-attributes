package com.reltio.rocs.delete.dto;

public enum Mode {
    SCAN_ONLY,
    SCAN_AND_ACTION,
    ACTION_ONLY,
    INVALID
}
