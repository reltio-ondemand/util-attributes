package com.reltio.rocs.delete.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.reltio.cst.domain.Attribute;

public class ScanResponseURI {
	
 	private List<Uri> objects;

	private Attribute cursor;
	
	@SerializedName("crosswalks")
	private List<Crosswalk> crosswalks;
	
	public Attribute getCursor() {
		return cursor;
	}

	public void setCursor(Attribute cursor) {
		this.cursor = cursor;
	}

	public List<Uri> getObjects() {
		return objects;
	}

	public void setObjects(List<Uri> objects) {
		this.objects = objects;
	}

	public List<Crosswalk> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalk> crosswalks) {
		this.crosswalks = crosswalks;
	}

}
