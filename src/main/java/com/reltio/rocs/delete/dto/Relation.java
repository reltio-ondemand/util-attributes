/**
 * 
 */
package com.reltio.rocs.delete.dto;

import java.util.List;

/**
 * @author spatil
 *
 */
public class Relation {

	private String type;

	private String uri;

	private List<Crosswalk> crosswalks;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public List<Crosswalk> getCrosswalks() {
		return crosswalks;
	}

	public void setCrosswalks(List<Crosswalk> crosswalks) {
		this.crosswalks = crosswalks;
	}
	
}
