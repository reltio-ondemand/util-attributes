package com.reltio.rocs.attribute.main;

import org.junit.Test;

public class AttributeExecuterTest {

    @Test
    public void scanOnlyAddNew() throws Exception {
        String args[] = {"src/test/resources/addNew/scan-only-configuration.properties"};
        AttributeExecuter.main(args);
    }

    @Test
    public void actionOnlyAddNew() throws Exception {
        String args[] = {"src/test/resources/addNew/action-only-configuration.properties"};
        AttributeExecuter.main(args);
    }


    @Test
    public void ScanAndActionOnlyAddNew() throws Exception {
        String args[] = {"src/test/resources/addNew/scan-and-action-only-configuration.properties"};
        AttributeExecuter.main(args);
    }


    @Test
    public void scanOnlyReplaceAll() throws Exception {
        String args[] = {"src/test/resources/replaceAll/scan-only-configuration.properties"};
        AttributeExecuter.main(args);
    }

    @Test
    public void actionOnlyReplaceAll() throws Exception {
        String args[] = {"src/test/resources/replaceAll/action-only-configuration.properties"};
        AttributeExecuter.main(args);
    }

    @Test
    public void scanAndActionOnlyReplaceAll() throws Exception {
        String args[] = {"src/test/resources/replaceAll/scan-and-action-only-configuration.properties"};
        AttributeExecuter.main(args);
    }

    @Test
    public void scanOnlyRemoveAll() throws Exception {
        String args[] = {"src/test/resources/removeAll/scan-only-configuration.properties"};
        AttributeExecuter.main(args);
    }

    @Test
    public void actionOnlyRemoveAll() throws Exception {
        String args[] = {"src/test/resources/removeAll/action-only-configuration.properties"};
        AttributeExecuter.main(args);
    }

    @Test
    public void scanAndActionOnlyRemoveAll() throws Exception {
        String args[] = {"src/test/resources/removeAll/scan-and-action-only-configuration.properties"};
        AttributeExecuter.main(args);
    }
}