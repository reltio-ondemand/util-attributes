#Quick Start

#Building

The main method of the application is at the following 
path: ./com/reltio/rocs/attribute/main/AttributeExecuter

#Dependencies

reltio-cst-core 1.4.7
slf4j-api 1.7.25

#Parameters File Example
```
ACTION=addnew
ENVIRONMENT_URL=sndbx.reltio.com
TENANT_ID=GwhNNYU1bNDD9XW
USERNAME=****
PASSWORD=****
#Either USERNAME, PASSWORD or CLIENT_CREDENTIALS to be used
#CLIENT_CREDENTIALS=
AUTH_URL=https://auth.reltio.com/oauth/token
OUTPUT_FILE=output.txt
#Either USERNAME, PASSWORD or CLIENT_CREDENTIALS to be used
#CLIENT_CREDENTIALS=
##input properties

SOURCE_SYSTEM=DPT
ATTRIBUTE_KEY=Prefix
ATTRIBUTE_VALUE=1000
ATTRIBUTE_OLD_VALUE=7878
ATTRIBUTE_NEW_VALUE=55555555
FILTER=equals(type,'configuration/entityTypes/Contact')

#Host of the proxy
HTTP_PROXY_HOST=
#Port for the proxy
HTTP_PROXY_PORT=
```

#How Input varies on action Specified 

```

Add below properties 
 
1) addNew - This will add new attribute 
ATTRIBUTE_KEY- Attribute name 
ATTRIBUTE_VALUE - Attribute value
2) findandremove - This action will scan all entities of type mentioned and finds the attribute is matching if so deletes the attribute 
	
ATTRIBUTE_KEY- Attribute name find and remove
ATTRIBUTE_VALUE - Attribute value to match for remove action
3) removeall - This action will remove all attributes of the key mentioned in configuration 
ATTRIBUTE_KEY- Attribute name to delete 
4) findandreplace -This action will find attributes and replace if the values is matching for the attribute values
ATTRIBUTE_OLD_VALUE-  Attribute existing value 
ATTRIBUTE_NEW_VALUE-  Attribute new value replace
5) replaceall - This will delete all values of the attribute
ATTRIBUTE_OLD_VALUE-  Attribute existing value 
ATTRIBUTE_NEW_VALUE-  Attribute new value replace

```

#Executing
Command to start the utility.
Note:Please note that use latest jar from jar folder in bitbucket

#Windows
java -jar reltio-util-attributes-${version}-jar-with-dependencies.jar configuration.properties > $logfilepath$'

#Unix
java -jar reltio-util-attributes-${version}-jar-with-dependencies.jar configuration.properties > $logfilepath$