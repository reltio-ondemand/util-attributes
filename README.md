
# BULK OPERATION FOR ATTRIBUTES

## Description
This utility Add/Update/Delete attributes of the filter spacified

1)	Can Add attribute to all entities of the entity types configured
2)	Can Update attribute to all entities of the entity types configured
3)	Update the attribute that value mentioned matching
4)	Can find and replace  attribute to all entities of the entity types configured
5)	Can delete all values of the attribute


##Change Log


```
#!plaintext

Version : 2.3.1
Updated date : 23/09/2019
Description : Client crendentials changes, validation, jar name change
```
```
#!plaintext

Version : 2.0.1
Updated date : 21/03/2019
Description : Password encryption changes
```

```
#!plaintext

Version : 2.0.0
LUD : 2nd Jan 2019
Description : Log Fix, Added OPS calcualtion,  Added new Properties MODE to have the user control over the operation
# MODE CAN BE OF THE FOLLOWING TYPES.

# 1) SCAN_ONLY : This does the the scan and writes the URI in the FILE
# 2) SCAN_AND_ACTION : This is Normal Action where the scan and action happens in the same execution
# 3) ACTION_ONLY : When scan_only is used subsequently we should use this ACTION_ONLY where we would input the uri and the action would be performed.

Last Update Date: 01/29/2018
Version: 1.0.0
Description: Initial version
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-attributes/src/8393a657968d89d9af803881210e4acd81d7163f/QuickStart.md?at=master&fileviewer=file-view-default).


